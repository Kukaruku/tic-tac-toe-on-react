import React, { createContext, useContext, useState } from "react";
import { Navigate, Route, BrowserRouter as Router, Routes } from "react-router-dom";
import { privateRoutes } from "./routes";
import Auth from "./components/pages/auth/auth";
import { IsAuthContext } from "./components/pages/auth/isAuthContext";

function App() {
  const [isAuth, setIsAuth] = useState(true)
  return (
    <IsAuthContext.Provider value = {{isAuth, setIsAuth}}>
    {isAuth
    ?
    <Router>
      <Routes>
        {privateRoutes.map(route => 
          <Route 
            element={route.element} 
            path={route.path} 
            exact={route.exact}
            key={route.path}
          />
          )}
          <Route path='*' key="/ratings" element={<Navigate to="/ratings"/>} />
      </Routes>
    </Router>
    :
    <Router>
      <Routes>
        <Route path={"/auth"} element={<Auth/>} />
        <Route path='*' element={<Navigate to="/auth"/>} />
      </Routes>
    </Router>
    }
  </IsAuthContext.Provider>
  );
}

export default App;
