import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import Ratings from './components/pages/ratings/ratings';
import Auth from './components/pages/auth/auth';
// import Ratings from './components/pages/ratings/ratings';
// import Auth from './components/pages/auth/auth.jsx';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <App/>
);


