import React from 'react';
import classes from './Table.module.css';

export const Row = ({children, height}) => {
    return (
        <tr className={classes.cell} style={{height: height + "px"}}>
            {children}
        </tr>
    );
};

export const Table = ({children}) => {
    return (
        <table>
            <tbody>
                {children}
            </tbody>
        </table>
    );
};

export const Td = ({children, width, center}) => {
    return (
        <>
            {center ?
                <td className={classes.center} style={{width: width + "px"}}>
                    {children}
                </td>
            : 
            <td style={{width: width + "px"}}>
                {children}
            </td>
            }
        </>

    );
};

