import React from 'react';
import classes from './MyLabel.module.css';
import MyLabel from './MyLabel';

const Gamer = ({children, dopClass, name, winRate, img, ...props}) => {
    
    return (
        <div className={classes.gamer_container} >
                <div className={classes.imgDiv}>
                    <img src={img} alt=""/>
                </div>
                <div>
                    <MyLabel stype="name" style={{fontSize: 20 + "px"}}>{name}</MyLabel>
                    <MyLabel stype="mini">{winRate}</MyLabel>
                </div>
        </div>
    );
};

export default Gamer;