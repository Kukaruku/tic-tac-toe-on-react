import React from 'react';
import classes from './MyLabel.module.css';

const MyLabel = ({children, stype, size, weight, mTop, mBot, ...props}) => {
    switch (stype){
        case "main":
            {
                return (
                    <div {...props} className={classes.label + " " + classes.main_label}>
                        {children}
                    </div>
                )
            }
        case "mini":
            {
                return (
                    <div {...props} className={classes.label + " " + classes.mini}>
                        {children}
                    </div>
                )
            }
        case "name":
            {
                return (
                    <div {...props} className={classes.label + " " + classes.name}>
                        {children}
                    </div>
                )
            }
        case "chat_name":
            {
                return (
                    <div {...props} className={classes.label + " " + classes.chat_name}>
                        {children}
                    </div>
                )
            }
        case "time":
            {
                return (
                    <div {...props} className={classes.label + " " + classes.time_label}>
                        {children}
                    </div>
                )
            }
        default :
            {
                return (
                    <div {...props}  className={classes.label} style={{fontSize: size + "px", fontWeight: weight + "px", marginTop: mTop + "px", marginBottom: mBot + "px" }}>
                        {children}
                    </div>
                );
            }   
    }


};

export default MyLabel;