import React from 'react';
import classes from './MyPage.module.css';

const MyPage = ({children, stype, ...props}) => {
    switch (stype){
        case "field":
            {
                return (
                    <div {...props} className={classes.page + " " + classes.field}>
                        {children}
                    </div>
                )
            }
        case "subj":
            {
                return (
                    <div {...props} className={classes.page + " " + classes.subj}>
                        {children}
                    </div>
                )
            }
        case "subj_wrap":
            {
                return (
                    <div {...props} className={classes.page + " " + classes.subj_wrap_page}>
                        {children}
                    </div>
                )
            }
        case "field_wrap":
            {
                return (
                    <div {...props} className={classes.page + " " + classes.field_wrap_page}>
                        {children}
                    </div>
                )
            }
        case "time":
            {
                return (
                    <div {...props} className={classes.page + " " + classes.time_container}>
                        {children}
                    </div>
                )
            }
        case "chat_wrap":
            {
                return (
                    <div {...props} className={classes.page + " " + classes.chat_wrap}>
                        {children}
                    </div>
                )
            }
        case "chat":
            {
                return (
                    <div {...props} className={classes.page + " " + classes.chat}>
                        {children}
                    </div>
                )
            }
        case "messages":
            {
                return (
                    <div {...props} className={classes.page + " " + classes.messages}>
                        {children}
                    </div>
                )
            }
        default:
            {
                return (
                    <div {...props} className={classes.page}>
                        {children}
                    </div>
                )
            }
    }
};

export default MyPage;