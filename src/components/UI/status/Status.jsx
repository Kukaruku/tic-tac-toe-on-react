import React from 'react';
import classes from './Status.module.css';
import MyLabel from '../label/MyLabel';

const Status = ({inGame, children, width, blocked, ...props}) => {
    if (inGame) {
        return (
            <label {...props} className={classes.status + " " + classes.active}>
                <MyLabel stype="name" style={{color: "white", fontSize: 16 + "px"}}>В игре</MyLabel>
            </label>
        );
    }
    if (blocked) {
        return (
            <label {...props} className={classes.status + " " + classes.blocked}>
                <MyLabel stype="name" style={{color: "white", fontSize: 16 + "px"}}>Заблокирован</MyLabel>
            </label>
        );
    }
    else {
        return (
            <label {...props} className={classes.status}>
                <MyLabel stype="name" style={{color: "white", fontSize: 16 + "px"}}>{children}</MyLabel>
            </label>
        );
    }
};

export default Status;