import React from 'react';
import classes from './Modal.module.css';
import MyButton from '../button/MyButton';
import MyContainer from '../container/MyContainer';
import trophy from "../../imgs/trophy.svg"
import MyLabel from '../label/MyLabel';
import MyInput from '../input/MyInput';
import Woman from "../../imgs/woman.svg";
import Man from "../../imgs/man.svg";
import Close from "../../imgs/close.svg"
import { useNavigate } from 'react-router-dom';

const Modal = ({active, winModal, addModal, setActive, winner}) => {

    const navigate = useNavigate();

    const rootClasses = [classes.myModal]

    if (active) {rootClasses.push(classes.active)}


    const WinModal = () => {
        return (
            <div className={classes.modal_content}>
                <img src={trophy} alt="" style={{paddingTop: 16 + "px"}}/>
                <MyLabel mTop={20} mBot={20}>{winner}</MyLabel>
                <MyButton style={{marginTop: 0, marginBottom: 12 + "px"}} onClick={() => {window.location.reload()}}>Новая игра</MyButton>
                <MyButton white style={{height: 48 + "px", width: 352 + "px"}} onClick={() => { navigate("/activePlayers");}} >Выйти в меню</MyButton>
            </div>
        );
    };

    const AddModal = () => {

        return (
            <div className={classes.modal_content} style={{gap:20 + "px"}}>
                <div className={classes.close_row}>
                    <div onClick={() => {setActive(false)}} className={classes.img}>
                        <img src={Close} alt="" />
                    </div>
                </div>
                <MyLabel style={{marginTop: 24 + "px", marginBottom: 20 + "px" }}>Добавьте игрока!</MyLabel>
                <div className={classes.inp_set}>
                    <MyLabel size={16} weight={500}>ФИО</MyLabel>
                    <MyInput width={352}>Иванов Иван Иванович</MyInput>
                </div>
                <div className={classes.row}>
                    <div className={classes.inp_set}>
                        <MyLabel size={16} widht={500}>Возраст</MyLabel>
                        <MyInput width={72}>0</MyInput>
                    </div>
                    <div className={classes.inp_set}>
                        <MyLabel size={16} widht={500}>Пол</MyLabel>
                        <div className={classes.row}>
                            <div className={classes.radio_form}>
                                <input type="radio" id="radio-1" value="1" name="radio" checked/>
                                <label for="radio-1"><img src={Woman} alt="" /></label>
                            </div>
                            <div className={classes.radio_form}>
                                <input type="radio" id="radio-2" value="2" name="radio"/>
                                <label for="radio-2"><img src={Man} alt="" /></label>
                            </div>
                        </div>
                    </div>
                </div>
                <MyButton style={{marginTop: 0, marginBottom: 0}}>Добавить</MyButton>
            </div>
        );
    };
    
    return (
        <div className={rootClasses.join(' ')}>
            <MyContainer stype="table" style={{padding: 40 + "px" +  24 + "px", paddingTop: 24 + "px"}}>
                {winModal ? <WinModal active={active}/> : <>{addModal ? <AddModal/> : <></>}</>}
            </MyContainer>
        </div>
    );
};

export default Modal;