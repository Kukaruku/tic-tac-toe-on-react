import React, { useState } from 'react';
import classes from './Switch.module.css';

const Switch = ({updateData}) => {
    const [checked, setCheked] = useState(false)
    return (
        <label className={classes.switch}>
            <input type="checkbox" checked={checked} onChange={() => {setCheked(!checked); updateData(checked)}}/>
            <span className={classes.track}/>
        </label>
    );
};

export default Switch;