import React from 'react';
import classes from './MyContainer.module.css';

const MyContainer = ({children, stype, ...props}) => {
    switch (stype){
        case "auth":
            {
                return (
                    <div {...props} className={classes.container + " " + classes.auth}>
                        {children}
                    </div>
                )
            }
        case "main":
            {
                return (
                    <div {...props} className={classes.container + " " + classes.main_label}>
                        {children}
                    </div>
                )
            }
        case "subj":
            {
                return (
                    <div {...props} className={classes.container + " " + classes.subj}>
                        {children}
                    </div>
                )
            }
        case "time":
            {
                return (
                    <div {...props} className={classes.container + " " + classes.time_container}>
                        {children}
                    </div>
                )
            }
        case "field":
            {
                return (
                    <div {...props} className={classes.container + " " + classes.game_field}>
                        {children}
                    </div>
                )
            }
        case "game_step":
            {
                return (
                    <div {...props} className={classes.container + " " + classes.game_step}>
                        {children}
                    </div>
                )
            }
        case "message":
            {
                return (
                    <div {...props} className={classes.container + " " + classes.message}>
                        {children}
                    </div>
                )
            }
        case "table":
            {
                return (
                    <div {...props} className={classes.container + " " + classes.table}>
                        {children}
                    </div>
                )
            }

        default:
            {
                return (
                    <div {...props} className={classes.container}>
                        {children}
                    </div>
                ) 
            }
    }
};

export default MyContainer;