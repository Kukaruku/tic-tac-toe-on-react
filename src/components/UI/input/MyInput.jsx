import React from 'react';
import classes from './MyInput.module.css';

const MyInput = React.forwardRef(({width, children, fg, ...props}, ref) => {
    return (
        <input placeholder={children} {...props} className={classes.auth} ref={ref} style={{width: width + "px", flexGrow: fg}}/>
    );
});

export default MyInput;