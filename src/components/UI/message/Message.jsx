import React from 'react';
import classes from './Message.module.css';
import MyLabel from '../label/MyLabel';
import MyContainer from '../container/MyContainer';

const Message = ({children, dopClass, mode, name, time, ...props}) => {
    if (mode === "o") {
        return (
            <div className={classes.wrap_o}>
                <MyContainer stype="message">
                    <div className={classes.name_container}>
                            <MyLabel stype="chat_name" style={{color: "var(--red)"}}>{name}</MyLabel>
                        <MyLabel stype="mini">{time}</MyLabel>
                    </div>
                    {children}
                </MyContainer>
            </div>
        );
    }
    if (mode === "x") {
        return (
            <div className={classes.wrap_x}>
                <MyContainer stype="message">
                    <div className={classes.name_container}>
                            <MyLabel stype="chat_name" style={{color: "var(--green)"}}>{name}</MyLabel>
                        <MyLabel stype="mini">{time}</MyLabel>
                    </div>
                    {children}
                </MyContainer>
            </div>
        );
    }

};

export default Message;