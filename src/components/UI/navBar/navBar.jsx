import React, { useContext, useState } from 'react';
import { NavLink, useNavigate } from "react-router-dom";
import classes from './NavBar.module.css';
import Logo from "../../imgs/s-logo.svg"
import SignOut from "../../imgs/signout-icon.svg"
import SignOutH from "../../imgs/signout-icon-hover.svg"
import { IsAuthContext } from '../../pages/auth/isAuthContext';


const NavBar = ({children, activeClass, ratings, gameField, activePlayers, historyPage, playersList, ...props}) => {
    const [img, setImg] = useState(SignOut)
    const {isAuth, setIsAuth} = useContext(IsAuthContext)
    const navigate = useNavigate();
    return (
<header>
    <div className={classes.logo}>
        <img src={Logo} alt=''/>
    </div>
    <div className={classes.nav_panel}>
        <div className={classes.nav_c + " " + gameField} onClick={function() {navigate("/gameField")}}>Игровое поле</div>
        <div className={classes.nav_c + " " + ratings} onClick={function() {navigate("/ratings")}}>Рейтинг</div>
        <div className={classes.nav_c + " " + activePlayers} onClick={function() {navigate("/activePlayers")}}>Активные игроки</div>
        <div className={classes.nav_c + " " + historyPage} onClick={function() {navigate("/history")}}>История игр</div>
        <div className={classes.nav_c + " " + playersList} onClick={function() {navigate("/playersList")}}>Список игроков</div>
    </div>
    <button className={classes.exitBtn} onClick={e => {setIsAuth(false)}} onMouseEnter={() => {setImg(SignOutH)}} onMouseLeave={() => {setImg(SignOut)}} ><img className={classes.img} src={img} alt=''/></button>
</header>
    );
};

export default NavBar;