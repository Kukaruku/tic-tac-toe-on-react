import React from 'react';
import classes from './MyButton.module.css';
import vector from '../../imgs/vector.svg'
import Icon from "../../imgs/stop-icon.svg"

const MyButton = ({children, mini, white, icon, ...props}) => {
    if (mini) {
        return (
            <button {...props} className={classes.btn_mini}>
                <img src={vector} alt="" />
            </button>
        );
    }
    if (white && !icon) {
        return (
            <button {...props} className={classes.btn_white}>
                {children}
            </button>
        );
    }
    if (icon && white) {
        return (
            <button {...props} className={classes.btn_white}>
                <img src={Icon} alt="" />
                {children}
            </button>
        );
    }
    else {
        return (
            <button {...props} className={classes.btn_login}>
                {children}
            </button>
        );
    }
};

export default MyButton;