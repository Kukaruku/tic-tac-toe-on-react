import React, {useState} from "react";
import MyContainer from "../../UI/container/MyContainer";
import NavBar from "../../UI/navBar/navBar";
import classes from "./ratings.module.css"
import ratings from "../../../ratings.json"
import navClasses from "../../UI/navBar/NavBar.module.css"
import MyPage from "../../UI/page/MyPage";
import MyLabel from "../../UI/label/MyLabel";


function Ratings() {
  return (
    <><NavBar ratings={navClasses.active} />
    <MyPage>
    <MyContainer stype="table">
        <MyLabel>Рейтинг игроков</MyLabel>
        <table>
            <tbody>
                <tr className={classes.cell}>
                    <td className={classes.fio + " " + classes.first}>ФИО</td>
                    <td className={classes.total + " " + classes.first}>Всего игр</td>
                    <td className={classes.total + " " + classes.first}>Победы</td>
                    <td className={classes.total + " " + classes.first}>Проигрыши</td>
                    <td className={classes.Winrate + " " + classes.first}>Процент побед</td>
                </tr>
                {ratings.map(rating => 
                        <tr className={classes.cell}>
                            <td className={classes.fio}>{rating.FIO}</td>
                            <td className={classes.total}>{rating.totalGames}</td>
                            <td className={classes.wins}>{rating.wins}</td>
                            <td className={classes.loses}>{rating.loses}</td>
                            <td className={classes.winrate}>{rating.winrate}</td>
                        </tr>
                    )}

            </tbody>
        </table>
    </MyContainer>
    </MyPage>
    </>
    )
}

export default Ratings;