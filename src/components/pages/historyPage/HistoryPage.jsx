import React, { useState } from "react";
import NavBar from "../../UI/navBar/navBar";
import navClasses from "../../UI/navBar/NavBar.module.css"
import MyPage from "../../UI/page/MyPage";
import MyContainer from "../../UI/container/MyContainer";
import MyLabel from "../../UI/label/MyLabel";
import classes from "./HistoryPage.module.css"
import imgO from "../../imgs/zero.svg"
import imgX from "../../imgs/x.svg"
import trophy from "../../imgs/trophy.svg"
import History from "../../../history.json"

function HistoryPage() {
  const [personWidth, setPersonWidth] = useState(214 + "px")
  return (
    <><NavBar historyPage={navClasses.active}/>
    <MyPage>
      <MyContainer stype="table">
        <MyLabel>История игр</MyLabel>
        <table>
          <tbody>
            <tr className={classes.cell}>
              <td>Игроки</td>
              <td>Дата</td>
              <td>Время игры</td>
            </tr>
            {History.map(history =>
              <tr className={classes.cell}>                
                  <td className={classes.gamer}>
                    <div className={classes.person} style={{width: personWidth}}>
                      <img src={imgO} alt="" className={classes.img}/>
                      <MyLabel stype="name">{history.oPlayer}</MyLabel>
                      {history.winner === "o" &&<img src={trophy} alt="" className={classes.img}/>}
                    </div>
                    <MyLabel stype="name" style={{fontWeight: 700}}>против</MyLabel>
                    <div className={classes.person}>
                      <img src={imgX} alt="" className={classes.img}/>
                      <MyLabel stype="name">{history.xPlayer}</MyLabel>
                      {history.winner === "x" &&<img src={trophy} alt="" className={classes.img}/>}
                    </div>
                  </td>
                  <td>
                    <MyLabel stype="name">{history.data}</MyLabel>
                  </td>
                  <td>
                    <MyLabel stype="name">{history.game_time}</MyLabel>
                  </td>
              </tr>  
            )}
          </tbody>
        </table>
      </MyContainer>
    </MyPage>
    </>
    )
}

export default HistoryPage;
