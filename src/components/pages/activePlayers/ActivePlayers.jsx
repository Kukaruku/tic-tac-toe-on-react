import React, { useState } from "react";
import classes from "./ActivePlayers.module.css"
import NavBar from "../../UI/navBar/navBar";
import navClasses from "../../UI/navBar/NavBar.module.css"
import MyPage from "../../UI/page/MyPage";
import MyContainer from "../../UI/container/MyContainer";
import MyLabel from "../../UI/label/MyLabel";
import Switch from "../../UI/switch/Switch";
import Status from "../../UI/status/Status";
import MyButton from "../../UI/button/MyButton";
import Players from "../../../activePlayers.json"

function ActivePlayers() {
  const [togled, setTogled] = useState(true);

  function updateData(value) {
    setTogled(value)
  }

  return (
    <><NavBar activePlayers={navClasses.active}/>
    <MyPage>
      <MyContainer stype="table" style={{gap: 0}}>
        <div className={classes.row}>
          <MyLabel>Активные игроки</MyLabel>
          <div style={{display: "flex", flexDirection: "row", gap: 8 + "px"}}>
            <MyLabel stype="name">Только свободные</MyLabel>
            <Switch updateData={updateData} />
          </div>
        </div>
        {togled ? <>
        {Players.map(players =>
          <div className={classes.row} key={players.name}>
            <MyLabel style={{width: 500 + "px"}} stype="name">{players.name}</MyLabel>
            {players.inGame ?
            <><Status inGame style={{marginRight: 80 + "px", marginLeft: 16 + "px"}}/>
            <MyButton disabled style={{width: 161 + "px", margin: 0}} >Позвать игрока</MyButton></>
            :
            <>
            <Status style={{marginRight: 80 + "px", marginLeft: 16 + "px"}}>Свободен</Status>
            <MyButton style={{width: 161 + "px", margin: 0}} >Позвать игрока</MyButton></>}
          </div>
        
        )} </>
        : <>
        {Players.map(players =>
          <>
          {!players.inGame ? 
          <div className={classes.row} key={players.name}>
            <MyLabel style={{width: 500 + "px"}} stype="name">{players.name}</MyLabel>
            <Status style={{marginRight: 80 + "px", marginLeft: 16 + "px"}}>Свободен</Status>
            <MyButton style={{width: 161 + "px", margin: 0}} >Позвать игрока</MyButton>
          </div>
          : <></>  
          } </>

        
        )} </>
        }
        
      </MyContainer>
    </MyPage>
    </>
    )
}

export default ActivePlayers;
