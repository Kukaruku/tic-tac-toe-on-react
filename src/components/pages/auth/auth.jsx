import React, { useContext } from "react";
import dog from "../../imgs/dog.svg";
import classes from "./auth.module.css";
import MyInput from "../../UI/input/MyInput";
import MyButton from "../../UI/button/MyButton";
import MyLabel from "../../UI/label/MyLabel";
import MyContainer from "../../UI/container/MyContainer";
import MyPage from "../../UI/page/MyPage";
import { useState } from "react";
import { useRef } from "react";
import { IsAuthContext } from "./isAuthContext"
import users from "../../../user.json"
import bcrypt from "bcryptjs";


function Auth() {
  const [login, setLogin] = useState("");
  const [pass, setPass] = useState("");
  const loginRef = useRef();
  const passRef = useRef();
  const [incorrectPass, setIncorrectPass] = useState(null);
  const [incorrectLogin, setIncorrectLogin] = useState(null);
  const {setIsAuth} = useContext(IsAuthContext)


  function updateValue(e) {
    let inputValue = e.target.value;
    return inputValue.replace(/[^A-Za-z0-9._]/, '');    
  }

  const checkAuth = (e) => {

    e.preventDefault()

    let finded = false;
    for (let i = 0; i < users.length; i++) {
        
        
        if (users[i].login === loginRef.current.value) {
            
            finded = true;

            if (bcrypt.compareSync(passRef.current.value, users[i].pass)) {
                
                setIsAuth(true)
                return;
            }
            
            else {
                setIncorrectPass(true);
                setIsAuth(false)
            }

        }
      
    }
    if (!finded) {
        setIncorrectLogin(true);
        setIsAuth(false)
    }
  }
  

  return (
    <MyPage style={{height: 100 + "vh", justifyContent: "center"}}>
      <MyContainer>
        <form className={classes.form} onSubmit={e => {checkAuth(e)}} method="post">
            <img className={classes.dog} src={dog} alt='' />
            <MyLabel stype="main">
              Войдите в игру
            </MyLabel>
            <div className={classes.auth_cont}>
                
                {incorrectLogin ?
                  <><MyInput width={352}  style={{borderColor: "#E93E3E"}} ref={loginRef} value={login} onChange = {e => setLogin(updateValue(e))} type="text" placeholder="Логин" id="auth_log" 
                  onPaste={e => {e.preventDefault(); return false;}} onCopy={e => {e.preventDefault(); return false;}} onCut={e => {e.preventDefault(); return false;}} onDrag={e => {e.preventDefault(); return false;}} onDrop={e => {e.preventDefault(); return false;}} />
                  <div className={classes.redText}>{"Неверный логин"}</div></>
                  : <MyInput width={352} ref={loginRef} value={login} onChange = {e => setLogin(updateValue(e))} type="text" placeholder="Логин" id="auth_log" 
                  onPaste={e => {e.preventDefault(); return false;}} onCopy={e => {e.preventDefault(); return false;}} onCut={e => {e.preventDefault(); return false;}} onDrag={e => {e.preventDefault(); return false;}} onDrop={e => {e.preventDefault(); return false;}} />
                }
            </div>
            <div className={classes.auth_cont}>
                {incorrectPass ?
                  <><MyInput width={352}  style={{borderColor: "#E93E3E"}} ref={passRef} value={pass} onChange = {e => setPass(updateValue(e))} type="password" placeholder="Пароль" id="auth_pass" />
                  <div className={classes.redText}>{"Неверный пароль"}</div></>
                  : <MyInput width={352}  ref={passRef} value={pass} onChange = {e => setPass(updateValue(e))} type="password" placeholder="Пароль" id="auth_pass" />
                }
            </div>
            <MyButton>Войти</MyButton>
        </form>
      </MyContainer>
    </MyPage>
  );
};

export default Auth;