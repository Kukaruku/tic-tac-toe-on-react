import React, { useState } from 'react';
import classes from './GameField.module.css';
import MyContainer from '../../UI/container/MyContainer';
import imageO from "../../imgs/zero.svg";
import imageX from "../../imgs/x.svg";
import imagexxlO from "../../imgs/xxl-zero.svg";
import imagexxlX from "../../imgs/xxl-x.svg";
import Modal from '../../UI/modal/Modal';

class GameField {
    state = [
        [null, null, null],
        [null, null, null],
        [null, null, null]
        ];
    mode = 'x';
    isOverGame = false;
    turnCount = 0;
    oWin = false;
    xWin = false;
    firstPlayer = 'Вячеслав Екатерина';
    secondPlayer = 'Владелен Пупкин';
    winner = "";

    getGameFieldStatus() {
        let all_rows = ' '
        for (let i = 0; i < 3; i ++) {
            for (let j = 0; j < 3; j ++) {
                if (gameField.state[i][j] === null) {
                    all_rows += '[ . ]      ';
                } 
                else {
                    all_rows += "[ " + gameField.state[i][j] + " ]";
                }
            }
            all_rows += '\n '
        }
        console.log(all_rows);
    }




    // Используется в след методе - проверка кто победил(из прошлого задания)
    winCheck (i,j, comboArr) {
        if (gameField.state[i][j] === 'x') {
            // comboArr.forEach((element) => {
            //     console.log(element)
            //     // document.getElementById(element).classList.add('xWin')
            // })
            this.xWin = true;
        }
        if (gameField.state[i][j] === 'o') {
            // comboArr.forEach((element) => {
            //     console.log(element)
            //     // document.getElementById(element).classList.add('oWin')
            // })
            this.oWin = true;
        }
    }

    winConfirm () {
        if (this.oWin) {
            this.winner = "Победил(а) " + this.secondPlayer + "!";
            this.isOverGame = true;
        }
        else if (this.xWin) {
            this.winner = "Победил(а) " + this.firstPlayer + "!";
            this.isOverGame = true;
        }
        else if (this.turnCount === 9) {
            this.winner = "Ничья!";
            this.isOverGame = true;
        }
    }

    // Логика проверки поля из прошлого задания
    xologicCheck () {

        for (let i = 0; i < 3; i ++) {
            for (let j = 0; j < 3; j ++) {
                //rows
                if (j === 1)  {
                    if (this.state[i][j-1] === this.state[i][j] & this.state[i][j-1] === this.state[i][j+1] &
                        this.state[i][j] != null) {
                            this.winCheck(i,j)
                            switch(i) {
                                case 0:
                                    return [0,3,6]
                                case 1:
                                    return [1,4,7]
                                default: 
                                    return [2,5,8]
                            }
                        }
                }
                //columns
                if (i === 1)  {
                    if (this.state[i-1][j] === this.state[i][j] & this.state[i-1][j] === this.state[i+1][j] &
                        this.state[i][j] != null) {
                            this.winCheck(i,j)
                            switch(j) {
                                case 0:
                                    return [0,1,2]
                                case 1:
                                    return [3,4,5]
                                default: 
                                    return [6,7,8]
                            }
                        }
                
                if (i === 1 & j === 1) {
                    //diagonal1 00 11 22
                    if (this.state[i-1][j-1] === this.state[i][j] & this.state[i-1][j-1] === this.state[i+1][j+1] &
                        this.state[i][j] != null) {
                            this.winCheck(i,j)
                            return [0, 4, 8]
                        }
    
                    //diagonal 02 11 20
                    if (this.state[i][j] === this.state[i-1][j+1] & this.state[i][j] === this.state[i+1][j-1] &
                        this.state[i][j] != null) {
                            this.winCheck(i,j)
                            return [2, 4, 6]
                        }
                    } 
                }
            }
        }
    }
}


const gameField = new GameField();


const XOGameField = ({updatePlayersNames}) => {
    
    
    
    // const addImg = (e) => {
    //     if (gameField.mode === 'x') {
    //         let xy = e.target.id;
    //         let cxy = xy.split(' ');
    //         gameField.fieldCellValue(cxy[0], cxy[1], e.target);
    //         gameField.getGameFieldStatus();
    //         gameField.xologicCheck(e.target);
    //         if (gameField.isOverGame) {
    //             allCells.forEach((element) => {
    //                 element.removeEventListener('click', addImg);
    //             });
    //             document.querySelector('.endModal').style.display = 'block';
    //         }
    
    //     }
    //     else {
    //         let xy = e.target.id;
    //         let cxy = xy.split(' ');
    //         gameField.fieldCellValue(cxy[0], cxy[1], e.target);
    //         gameField.getGameFieldStatus();
    //         gameField.xologicCheck();
    //         if (gameField.isOverGame) {
    //             allCells.forEach((element) => {
    //                 element.removeEventListener('click', addImg);
    //             });
    //             document.querySelector('.endModal').style.display = 'block';
    //         }
    //     }
    // }
    
    // let cell = document.getElementById('cell1')
    // cell.addEventListener('click', addImg)
    
    // let allCells = document.querySelectorAll('.cell')
    // let imgTurn = document.querySelector('.imgTurn')
    // let nameTurn = document.querySelector('.nameTurn')
    
    // document.getElementById('newGame').addEventListener('click', function newG() {
    //     location.reload();
    // })
    
    // allCells.forEach((element) => {
    //     element.addEventListener('click', addImg);
    // });



    const [field, setField] = useState(Array(9).fill(null))
    const [stepImg, setStepImg] = useState(imageX)
    const [fieldActive, setFieldActive] = useState(Array(9).fill(""))
    const [activePlayer, setActivePlayer] = useState(gameField.firstPlayer)


    updatePlayersNames(gameField.firstPlayer, gameField.secondPlayer)

    const setImg = (SqNumber, x, y) => {

        const fieldArray = field.slice();  
        const fieldACtiveArray = fieldActive.slice();
        
        if (gameField.state[y][x] === null && !gameField.isOverGame) {
            if (gameField.mode === 'x') {
                gameField.state[y][x] = "x";
                gameField.turnCount ++;
                fieldArray[SqNumber] = imagexxlX;
                setStepImg(imageO);
                setActivePlayer(gameField.secondPlayer)
                gameField.mode = 'o';
                setField(fieldArray)
                
            }
            else {
                gameField.state[y][x] = "o";
                gameField.turnCount ++;
                fieldArray[SqNumber] = imagexxlO;
                setStepImg(imageX);
                setActivePlayer(gameField.firstPlayer)
                gameField.mode = 'x';
                setField(fieldArray)
            }
        }
        let winCombo = gameField.xologicCheck();
        gameField.winConfirm();
        if (gameField.isOverGame && gameField.xWin) {
            winCombo.forEach(element => {
                fieldACtiveArray[element] = "xWin"
            });
        }
        if (gameField.isOverGame && gameField.oWin) {
            winCombo.forEach(element => {
                fieldACtiveArray[element] = "oWin"
            });
        }

        setFieldActive(fieldACtiveArray);
    }

    const Square = ({onSquareClick, value, active}) => {
        const rootClasses = [classes.cell];
        active === "xWin" && rootClasses.push(classes.xWin);
        active === "oWin" && rootClasses.push(classes.oWin);
        return (
            <div className={rootClasses.join(' ')} onClick={onSquareClick}>
                {value && <img src={value} alt="" />}
            </div>
        );
    }

    const GameStep = ({player}) => {
        return (        
        <div className={classes.game_step_wrap}>   
            <MyContainer stype="game_step">
                Ходит <img src={stepImg} alt=''/> {player}
            </MyContainer>
        </div>
        )
    }


    return (
        <> {gameField.isOverGame && <Modal winModal active winner={gameField.winner}/>}
        <div className={classes.field_wrap}>
            <MyContainer stype="field">
                <div className={classes.row}>
                    <Square value={field[0]} active={fieldActive[0]} onSquareClick={() => setImg(0, 0, 0)}/>
                    <Square value={field[1]} active={fieldActive[1]} onSquareClick={() => setImg(1, 0, 1)}/>
                    <Square value={field[2]} active={fieldActive[2]} onSquareClick={() => setImg(2, 0, 2)}/>
                </div>
                <div className={classes.row}>
                    <Square value={field[3]} active={fieldActive[3]} onSquareClick={() => setImg(3, 1, 0)}/>
                    <Square value={field[4]} active={fieldActive[4]} onSquareClick={() => setImg(4, 1, 1)}/>
                    <Square value={field[5]} active={fieldActive[5]} onSquareClick={() => setImg(5, 1, 2)}/>
                </div>
                <div className={classes.row}>
                    <Square value={field[6]} active={fieldActive[6]} onSquareClick={() => setImg(6, 2, 0)}/>
                    <Square value={field[7]} active={fieldActive[7]} onSquareClick={() => setImg(7, 2, 1)}/>
                    <Square value={field[8]} active={fieldActive[8]} onSquareClick={() => setImg(8, 2, 2)}/>
                </div>
            </MyContainer>
        </div>
        <GameStep player={activePlayer} /></>
    );
};

export default XOGameField;