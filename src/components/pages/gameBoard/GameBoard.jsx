import React, { useState } from "react";
import NavBar from "../../UI/navBar/navBar";
import navClasses from "../../UI/navBar/NavBar.module.css"
import MyPage from "../../UI/page/MyPage";
import classes from "./GameField.module.css"
import MyContainer from "../../UI/container/MyContainer";
import MyLabel from "../../UI/label/MyLabel";
import MyInput from "../../UI/input/MyInput";
import MyButton from "../../UI/button/MyButton";
import imageO from "../../imgs/zero.svg";
import imageX from "../../imgs/x.svg";
import Gamer from "../../UI/label/gamers";
import Message from "../../UI/message/Message";
import GameField from "./GameField";

const GameBoard = () => {

    const [firstPlayer, setFirstPlayer] = useState("")
    const [secondPlayer, setSecondPlayer] = useState("")
    
    
    const updatePlayersNames = (firstPlayer, secondPlayer) => {
        setFirstPlayer(firstPlayer)
        setSecondPlayer(secondPlayer)
    }

  return (
    <><NavBar gameField={navClasses.active}/>
    <MyPage stype="field">
        <MyPage stype="subj_wrap">
            <MyContainer stype="subj">
                <MyLabel stype="main" style={{marginTop: 0, display: "flex", alignItems: "flex-start"}}>Игроки</MyLabel>
                <Gamer name={secondPlayer} winRate="29%" img={imageO}/>
                <Gamer name={firstPlayer} winRate="71%" img={imageX}/>
            </MyContainer>
        </MyPage>

        <MyPage stype="field_wrap">
                <div className={classes.time_wrap}>
                    <MyContainer stype="time">
                        <MyLabel stype="time">05:12</MyLabel>
                    </MyContainer>
                </div>
                <GameField updatePlayersNames={updatePlayersNames}/>
        </MyPage>
        <MyPage stype="chat_wrap">
                <MyPage stype="chat">
                    <MyPage stype="messages">
                        <div className={classes.messageX}>
                            <Message
                                name={'Добрыня'}
                                time={'4:19'}
                                mode={'x'}
                            >
                                Ну что, готовься к поражению!!1
                            </Message>
                        </div>
                        <div className={classes.messageO}>
                            <Message
                                name={'Антошка Попович'}
                                time={'4:20'}
                                mode={'o'}
                            >
                                Надо было играть за крестики. Розовый — мой не самый счастливый цвет
                            </Message>
                        </div>
                        <div className={classes.messageX}>
                            <Message
                                name={'Добрыня'}
                                time={'4:19'}
                                mode={'x'}
                            >
                                Ну что, готовься к поражению!!1
                            </Message>
                        </div>
                        <div className={classes.messageO}>
                            <Message
                                name={'Антошка Попович'}
                                time={'4:20'}
                                mode={'o'}
                            >
                                Надо было играть за крестики. Розовый — мой не самый счастливый цвет
                            </Message>
                        </div>
                        <div className={classes.messageX}>
                            <Message
                                name={'Добрыня'}
                                time={'4:19'}
                                mode={'x'}
                            >
                                Ну что, готовься к поражению!!1
                            </Message>
                        </div>
                        <div className={classes.messageO}>
                            <Message
                                name={'Антошка Попович'}
                                time={'4:20'}
                                mode={'o'}
                            >
                                Надо было играть за крестики. Розовый — мой не самый счастливый цвет
                            </Message>
                        </div>
                        <div className={classes.messageX}>
                            <Message
                                name={'Добрыня'}
                                time={'4:19'}
                                mode={'x'}
                            >
                                Ну что, готовься к поражению!!1
                            </Message>
                        </div>
                        <div className={classes.messageO}>
                            <Message
                                name={'Антошка Попович'}
                                time={'4:20'}
                                mode={'o'}
                            >
                                Надо было играть за крестики. Розовый — мой не самый счастливый цвет
                            </Message>
                        </div>
                    </MyPage>
                    <MyPage style={{gap: 12 + "px", width: 100+"%"}}>
                        <MyInput fg={1}>Сообщение...</MyInput>
                        <MyButton mini></MyButton>
                    </MyPage>
                </MyPage>
            </MyPage>
    </MyPage>
    </>
    )
}

export default GameBoard;
