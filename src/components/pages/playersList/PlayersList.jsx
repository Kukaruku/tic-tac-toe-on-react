import React, { useState } from "react";
import NavBar from "../../UI/navBar/navBar";
import navClasses from "../../UI/navBar/NavBar.module.css"
import classes from "./PlayersList.module.css"
import MyPage from "../../UI/page/MyPage";
import MyContainer from "../../UI/container/MyContainer";
import MyLabel from "../../UI/label/MyLabel";
import MyButton from "../../UI/button/MyButton";
import { Row, Table, Td } from "../../UI/table/Table";
import Woman from "../../imgs/woman.svg";
import Man from "../../imgs/man.svg";
import Status from "../../UI/status/Status";
import Players from "../../../playerList.json"
import Modal from "../../UI/modal/Modal";

function PlayersList() {
  const [modal, setModal] = useState("")

  return (
    <>
    <NavBar playersList={navClasses.active}/>
    <MyPage>
      <Modal active={modal} setActive={setModal} addModal></Modal>
      <MyContainer stype="table">
        <div className={classes.row}>
          <MyLabel>Список игроков</MyLabel>
          <MyButton onClick={() => {setModal(true)}} style={{margin: 0, width: 173 + "px"}}>Добавить игрока</MyButton>
        </div>
        <Table>
          <Row>
            <Td width={440}>ФИО</Td>
            <Td width={108}>Возраст</Td>
            <Td width={46} center>Пол</Td>
            <Td width={152}>Статуc</Td>
            <Td width={160}>Создан</Td>
            <Td width={160}>Изменен</Td>
            <Td width={190}></Td>
          </Row>
          {Players.map(players => 
            <Row  height={64}>
              <Td width={440}><MyLabel stype="name">{players.name}</MyLabel></Td>
              <Td width={108}><MyLabel stype="name">{players.age}</MyLabel></Td>
              <Td width={46} center>
                {players.gender === "woman" ? <img src={Woman} alt="" /> : <img src={Man} alt="" />}
              </Td>
              <Td width={152}>
                {players.isBanned ? <Status style={{width: 136 + "px"}}>Активен</Status> : <Status blocked style={{width: 136 + "px"}}></Status>}
              </Td>
              <Td width={160}><MyLabel stype="name">12 октября 2021</MyLabel></Td>
              <Td width={160}><MyLabel stype="name">12 октября 2021</MyLabel></Td>
              <Td width={190} center>
                {players.isBanned ? <MyButton white>Заблокировать</MyButton>
                :  <MyButton white icon>Разблокировать</MyButton>}
              </Td>
            </Row>
          )}

        </Table>
        <table>
          <tbody>
            <tr className={classes.cell}>

            </tr>
          </tbody>
        </table>
      </MyContainer>
    </MyPage>
    </>
    )
}

export default PlayersList;
