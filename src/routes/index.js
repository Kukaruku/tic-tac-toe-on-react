import ActivePlayers from "../components/pages/activePlayers/ActivePlayers"
import GameBoard from "../components/pages/gameBoard/GameBoard"
import HistoryPage from "../components/pages/historyPage/HistoryPage"
import PlayersList from "../components/pages/playersList/PlayersList"
import Ratings from "../components/pages/ratings/ratings"


export const privateRoutes = [
    {path: '/gameField', element: <GameBoard/>, exact: true},
    {path: '/ratings', element: <Ratings/>, exact: true},
    {path: '/activePlayers', element: <ActivePlayers/>, exact: true},
    {path: '/history', element: <HistoryPage/>, exact: true},
    {path: '/playersList', element: <PlayersList/>, exact: true}
]